import scrapy
from urllib.parse import urljoin
import datetime
import pandas as pd
import os
import sys
module_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(module_dir)
print(module_dir)
from bienImmobilier import BienImmobilier
import csv
import time

class LeaderImmoSpider(scrapy.Spider):
    name = "LeaderImmoSpider"
    allowed_domains = ["www.leaders-immo.com"]
    start_urls = ["https://www.leaders-immo.com/vente","https://www.leaders-immo.com/louer"]
    custom_settings = {        
        'DOWNLOAD_DELAY': 4, # 10 seconds delay
        'RETRY_TIMES': 3,
        'RETRY_HTTP_CODES': [500, 502, 503, 504, 400, 408]
    }
    

    def parse(self, response):       
        # links = response.css('a.property-row-picture-target::attr(href)').extract()
        links = response.css('div.box.px-2 > div.card.hover-change-image > a::attr(href)').getall()

        print(links)
        print("___________________________")
        time.sleep(10)
        Url_List=[]
        count=0
        b=BienImmobilier()         
        for link in links:
            path="https://www.leaders-immo.com"+link
            # print(path)
            
            if b.ReadbyUrl(path):
               count+=1
               print(count)
               continue
               
            else:
               Url_List.append(path)

        for link in Url_List:
           yield scrapy.Request(url=response.urljoin(link), callback=self.parse_details, meta={'url': response.urljoin(link)})
        
        if count>=9:
           return
     

        next_page = response.css('a.page-link[rel="next"]::attr(href)').get() 
            
        if next_page:
          next_page="https://www.leaders-immo.com"+next_page
          
          yield scrapy.Request(url=urljoin(response.url, next_page), callback=self.parse)
    
    def parse_details(self, response):
       code=response.css('h1.property-title::text').get()
       code=code.replace("httpsRéférence","").replace(" ","").replace(":","")
       

       url=response.url
        
       offre = response.css('span.featured-type::text').get()
       type = response.css('span.featured-type-cat::text').get()
        
       price = response.css('span.price-text::text').get().strip()
       desc = response.css('div.description-inner-wrapper p::text').get()
       images= response.css('a.p-popup-image::attr(href)').getall()
       SuperficieTotal=None
       SuperficieConst=None
       Chambres=None
       Sallesbain=None
       state=None
       ville=None
       pays=None


       span_values = response.css('div.property-detail-metas span.value-suffix')
       for span in span_values:
            text = span.css('::text').get()
            # print(text)
       span_values = response.css('div.property-detail-metas span.value-suffix')
       
       
       for span in span_values:
            text = ''.join(span.css('::text').getall())
            desc=desc+" "+text
            # print(text)
            if "Superficie Total" in text: SuperficieTotal=text.replace("Superficie Total","").replace("m²","").replace(" ","")
            if "Superficie constructible" in text: SuperficieConst=text.replace("Superficie constructible","").replace("m²","").replace(" ","")
            # print(SuperficieTotal,SuperficieConst)


   
       
       
       lis = response.css('ul.list li')
       for li in lis:
            
            text = li.css('div.text::text').get()
            value = li.css('div.value::text').get()
            text=text.strip()
            if value:
             desc=desc+" "+ text+": "+value+" "
            if "Superficie Total" in text: SuperficieTotal=value.replace("Superficie Total","").replace("M²","").replace(" ","").replace(":","")
            if "Superficie Couvert" in text: SuperficieConst=value.replace("Superficie Couvert","").replace("M²","").replace(" ","").replace(":","")
            if "chambre:" in text: Chambres=value.replace("Nombre de chambre","").replace(" ","").replace(":","")
            if "bain" in text: Sallesbain=value.replace("Nombre de salle de bain","").replace(" ","").replace(":","")
            if "Ville" in text: state=value.replace("Ville","").replace(" ","").replace(":","")
            if "Délégation" in text: ville=value.replace("Délégation","").replace(" ","").replace(":","")
            if "Pays" in text: pays=value.replace("Pays","").replace(" ","").replace(":","") 
            # print(text,": ",value)
       
       
       now = datetime.datetime.now()
       ScrapedDate = now.strftime("%d-%m-%y %H:%M:%S") 
       ul_elements = response.css('ul.columns-gap')
       for ul in ul_elements:
            li_texts = ul.css('li::text').getall()
            for text in li_texts:
                text=text.strip() 
                desc=" "+desc+" "+text
       desc = " ".join(desc.split())
 
       row ={"url": response.url,'Code':code,"description": desc,"state":state,"ville":ville,"pays":pays
             ,'surface_totale': SuperficieTotal,'surface_habitable': SuperficieConst,'price': price,
            "Nb_chambre":Chambres,"Nb_SalleBain":Sallesbain,
            'image_urls': images,'ScrapedDate':ScrapedDate,'offre':offre,'type':type}

   
        
          
       b=BienImmobilier()
       b.extractLeaderImmo(row)
       b.noneCheck()
       b.print_maison()
       b.SaveDb(b)
       

