import time
import scrapy
from scrapy.http import HtmlResponse
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import re
import json
from geopy.geocoders import Nominatim
import emoji
import chardet
import unicodedata
from bienImmobilier import BienImmobilier
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium import webdriver
from selenium.webdriver.edge.options import Options as EdgeOptions
import time
import datetime
import keyboard
from googletrans import Translator
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import MoveTargetOutOfBoundsException
import os
class FacebookSpider(scrapy.Spider):
    name = 'FacebookSpider'
    allowed_domains = ['www.facebook.com']
    start_urls = ['http://www.facebook.com']

    custom_settings = {
        
        'DOWNLOAD_DELAY': 4, # 10 seconds delay
        'RETRY_TIMES': 3,
        'RETRY_HTTP_CODES': [500, 502, 503, 504, 400, 408]

    }

    def __init__(self):
        self.links=[ #Marketplace links
           'https://www.facebook.com/marketplace/111663698852329/propertyforsale', #groups links
                    'https://www.facebook.com/groups/walidbourogaa?sorting_setting=CHRONOLOGICAL_LISTINGS',
                    'https://www.facebook.com/groups/1584726631769870?sorting_setting=CHRONOLOGICAL_LISTINGS',
                    'https://www.facebook.com/groups/679094669105325?sorting_setting=CHRONOLOGICAL_LISTINGS',
                    'https://www.facebook.com/groups/hamami.moutiia?sorting_setting=CHRONOLOGICAL_LISTINGS',
                    'https://www.facebook.com/groups/500047244205244?sorting_setting=CHRONOLOGICAL_LISTINGS',
                    'https://www.facebook.com/groups/BnBTunisie?sorting_setting=CHRONOLOGICAL_LISTINGS',
                    'https://www.facebook.com/groups/656964197661792?sorting_setting=CHRONOLOGICAL_LISTINGS',
                    'https://www.facebook.com/groups/875460466244388?sorting_setting=CHRONOLOGICAL',
                    'https://www.facebook.com/groups/1757769681201761?sorting_setting=CHRONOLOGICAL_LISTINGS',
                    'https://www.facebook.com/groups/223393377797045?sorting_setting=CHRONOLOGICAL_LISTINGS',
                    'https://www.facebook.com/groups/immobiliere.tn?sorting_setting=CHRONOLOGICAL_LISTINGS',
                    'https://www.facebook.com/groups/392413620966884?sorting_setting=CHRONOLOGICAL_LISTINGS',
                    'https://www.facebook.com/groups/59810753589?sorting_setting=CHRONOLOGICAL_LISTINGS',
                    'https://www.facebook.com/groups/1844918752349514?sorting_setting=CHRONOLOGICAL_LISTINGS',
                    'https://www.facebook.com/groups/424998871291965?sorting_setting=CHRONOLOGICAL_LISTINGS',
                    'https://www.facebook.com/groups/693650537411391?sorting_setting=CHRONOLOGICAL',
                    'https://www.facebook.com/groups/644648282301971?sorting_setting=CHRONOLOGICAL',
                    'https://www.facebook.com/groups/303199895004471?sorting_setting=CHRONOLOGICAL',
                    'https://www.facebook.com/groups/1421187528123087?sorting_setting=CHRONOLOGICAL',
                    'https://www.facebook.com/groups/1668207853418909?sorting_setting=CHRONOLOGICAL',
                    'https://www.facebook.com/groups/annoncesimmobiliersentunisie?sorting_setting=CHRONOLOGICAL_LISTINGS',
                    'https://www.facebook.com/groups/755753378104065?sorting_setting=CHRONOLOGICAL_LISTINGS',
                    'https://www.facebook.com/groups/529441487757615?sorting_setting=CHRONOLOGICAL_LISTINGS',
                    'https://www.facebook.com/groups/875460466244388?sorting_setting=CHRONOLOGICAL']
        #browser settings
        options = webdriver.EdgeOptions()
        options.use_chromium = True
        prefs = {"profile.default_content_setting_values.notifications" : 2}
        options.add_experimental_option("prefs",prefs)
        current_directory = os.getcwd()+'/msedgedriver.exe'
        
        driver_path = current_directory.replace("\\", "/")
        
        
        options.add_argument("--disable-gpu")
        options.add_argument(f"--driver-path={driver_path}")
        self.driver = webdriver.Edge(options=options)
        

    def parse(self, response):
       
        #Login

        self.driver = webdriver.Edge()
        
        self.driver.get("https://www.facebook.com/login/")
   
          
        username = WebDriverWait( self.driver, 10).until(EC.element_to_be_clickable((By.CSS_SELECTOR, "input[name='email']")))
        password = WebDriverWait( self.driver, 10).until(EC.element_to_be_clickable((By.CSS_SELECTOR, "input[name='pass']")))
        time.sleep(2)
        #enter username and password 
        username.clear()
        username.send_keys("edsoftad@gmail.com")
        password.clear()
        password.send_keys("bl#25059594")
        #logging in
        WebDriverWait( self.driver, 2).until(EC.element_to_be_clickable((By.CSS_SELECTOR, "button[type='submit']"))).click()

        time.sleep(5)
        self.driver.get(response.url)
         
        

        for link in self.links:
         self.driver.get(link)
         b=BienImmobilier()
         if "marketplace" in link:
            i=0                        
            while i==0:
                i+=1
                #Scrollingdown
                self.driver.execute_script('window.scrollTo(0, document.body.scrollHeight);')
                time.sleep(2) 
                response = HtmlResponse(url=self.driver.current_url,body=self.driver.page_source,encoding='utf-8')   
                #getting html src code 
                body=self.driver.page_source
                sel = scrapy.Selector(response)
                time.sleep(10)
                matches=[]
                while'"listing":{"__typename":"GroupCommerceProductItem","id":' in body:    
                    start = '"listing":{"__typename":"GroupCommerceProductItem","id":"'
                    index = body.index(start)
                    new_text = body[index+len(start):]
                    end='","primary_listing_photo'
                    indexEnd=new_text.index(end)
                    code=new_text[:indexEnd]
                    matches.append(code)
                    body=new_text
                annonces=sel.xpath("//div[@class='listing-item-container']/a[contains(@class, 'listing-item')]")
                for annonce in annonces:
                    link = annonce.xpath('@href').extract()[0]
                    if link not in self.links:
                        self.links.append(link)
                        self.count+=1
                        yield scrapy.Request(link, callback=self.parse_annonce)
                    time.sleep(2)
                 
                for match in matches:
                    path="https://www.facebook.com/marketplace/item/"+match+"/"
                    if not b.ReadbyUrl(path):
                     self.parse_annonce(path)
                    time.sleep(2)

         elif "groups" in link and not "posts" in link :
            time.sleep(5)
            actions = ActionChains(self.driver)
            try:
             actions.move_by_offset(1, 1)   
             actions.click()
             actions.perform()
            except MoveTargetOutOfBoundsException as e:
     
             print("MoveTargetOutOfBoundsException occurred:", e) 
             
            i=0
           
            time.sleep(10) 
            response = HtmlResponse(url=self.driver.current_url,body=self.driver.page_source,encoding='utf-8')    
            body=self.driver.page_source
            sel = scrapy.Selector(response)
             
            
           
            time.sleep(4)
            href_values = []
            x=0
            while x<5:#5
             x+=1
             
             time.sleep(4)
              

             while i<10:#10
                i+=1
                keyboard.press_and_release('page down')
                time.sleep(5) 
             response = HtmlResponse(url=self.driver.current_url,body=self.driver.page_source,encoding='utf-8')    
             body=self.driver.page_source
             elements = self.driver.find_elements(By.XPATH,"//span[@class='x4k7w5x x1h91t0o x1h9r5lt x1jfb8zj xv2umb2 x1beo9mf xaigb6o x12ejxvf x3igimt xarpa2k xedcshv x1lytzrv x1t2pt76 x7ja8zs x1qrby5j']")
             skip=0
             for element in elements:
                # Perform hover action
                skip+=1
                if element.is_displayed() and skip<len(elements)-3:
                 actions = ActionChains(self.driver)
                 actions.move_to_element(element).perform()

                # Get attributes if found
                 attributes = element.get_attribute("outerHTML")
                 if attributes:
                     # Find and hover over the nested <a> tag
                    try:
                     anchor_tag = element.find_element(By.TAG_NAME,'a')
                     if anchor_tag.is_displayed():
                      actions.move_to_element(anchor_tag).perform()
                      time.sleep(4)
                    

                    # Get href attribute if found
                     href = anchor_tag.get_attribute('href')
                     if href and "/posts/" in href and href not in href_values:
                        
                        with open('debug.txt', 'a') as file:
                         file.write(link+"-"+href+"\n")
                        
                        href_values.append(href)
                     else:
                        print("No href attribute found for the <a> tag.")
                    except NoSuchElementException:
                     print("No <a> tag found within the element.")
                 else:
                    print("No attributes found for the element.")

             time.sleep(3)
 
            
            for href_value in href_values:
                if not b.ReadbyUrl(href_value):
                    self.parse_grp_annonce(href_value)

         else:pass
     
        self.driver.close() 

    def parse_annonce(self,url):        
        self.driver.get(url)
        time.sleep(10)
        body=self.driver.page_source       
        start = 'marketplace_listing_renderable_target":'
        index = body.index(start)
        new_text = body[index+len(start):]
        end=',"is_shipping_offered'
        indexEnd=new_text.index(end)
        code=new_text[:indexEnd]
        code=str(code)+"}"
        data = json.loads(code )
        latitude = data['location']['latitude']
        longitude = data['location']['longitude']
        geolocator = Nominatim(user_agent="<your user agent>")
        location = geolocator.reverse(f"{latitude}, {longitude}",language='fr')
        fulladress=location.address
        adresses=location.address.split(",")
 
        ####Price
        start = '"listing_price":{"amount":"'
        index = body.index(start)
        new_text = body[index+len(start):]
        end='","currency":'
        indexEnd=new_text.index(end)
        code=new_text[:indexEnd]
        price=str(code)

        ####images
        start = 'class="xu1mrb x1yyh9jt x1jx8tsq"'
        index = body.index(start)
        new_text=body[index:]
        # print(new_text)
        end='Partager'
        indexEnd=new_text.index(end)
        new_text = new_text[:indexEnd]
        images=[]
        while 'src="' in new_text:
            # print("in")
            start = 'src="'
            index = new_text.index(start)
            text = new_text[index+len(start):]
            end='"'
            indexEnd=text.index(end)
            img=text[:indexEnd]
            img=img.replace("amp;","")
            images.append(str(img))
            new_text=text
        
        

        ####Description
        start = '"redacted_description":{"text":"'
        index = body.index(start)
        new_text = body[index+len(start):]
        end='},"__isMarketplaceRealEstateListing"'
        indexEnd=new_text.index(end)
        code=new_text[:indexEnd]
        code=str(code)
        # print(code)
        pattern = r'\b[a-zA-Z]{6,}\b'
        matches = re.findall(pattern, code)
        description=" "
        if len(matches)>0:
            try:
                text = re.sub(r'[\ud800-\udbff][\udc00-\udfff]', '<?>', code)
                text = text.encode("utf-8", "ignore").decode("utf-8")
                text = text.encode("utf-8")
                text = text.decode("utf-8", "surrogatepass")
                description=text
            except Exception as e:
             print("An error occurred:", e)
        else:
            text = re.sub(r'\\ud[8-9a-f][0-9a-f]{2}|\\ud[a-f][0-9a-f]{3}', '', code)
            description = text.encode('utf-8').decode('unicode_escape')


        now = datetime.datetime.now()
        ScrapedDate = now.strftime("%d-%m-%Y %H:%M:%S") 

        row = {"url": url,"description": description+" ..",'address': fulladress,'location': adresses,'price': price, 
            'image_urls': images ,"ScrapedDate":ScrapedDate,"inserteddate":ScrapedDate       
        }
        b=BienImmobilier()
        b.extractFB(row)
        b.noneCheck()
        b.print_maison()
        b.SaveDb(b)

    def parse_grp_annonce(self,url):
        
            self.driver.get(url)
            response = HtmlResponse(url=self.driver.current_url,body=self.driver.page_source,encoding='utf-8')    
            time.sleep(15) 
            body=self.driver.page_source
            sel = scrapy.Selector(response)
            actions = ActionChains(self.driver)
            try:
             actions.move_by_offset(1, 1)   
             actions.click()
             actions.perform() 
            except MoveTargetOutOfBoundsException as e:
             print("MoveTargetOutOfBoundsException occurred:", e)
            time.sleep(5)
            i=0
            while i<8:
                i+=1
                keyboard.press_and_release('page down')
                time.sleep(2)


            ###########images 
            new_text=""
            start = 'x1ey2m1c xds687c x5yr21d x10l6tqk x17qophe x13vifvy xh8yej3'
            if start in new_text:
             index = body.index(start)
             new_text=body[index:]

            images=[]
            while 'x1ey2m1c xds687c x5yr21d x10l6tqk x17qophe x13vifvy xh8yej3' in new_text:
                
                start = 'x1ey2m1c xds687c x5yr21d x10l6tqk x17qophe x13vifvy xh8yej3'
                index = new_text.index(start)
                img = new_text[index+len(start):index+len(start)+400]
                
                img=img.replace("amp;","").replace('" referrerpolicy="origin-when-cross-origin" src="','').replace('"></div></div><di','').replace("xl1xv1r","").replace(" ","").replace("></div>","").replace('"','')

                images.append(str(img))
                new_text=new_text[index+len(start+img):]
            
            
            
            
            
            
 
            #Description
            title=""
            if 'title_with_entities' in body:
               start = 'title_with_entities":{"text":"'
               index = body.index(start)
               new_text=body[index+len(start):]
               end='"'
               endindex=new_text.index(end)
               print("\n\title:")   
               print("\n\title:", new_text)
               title=new_text[:endindex]
            #wors
            try:
                elements = self.driver.find_elements(By.CSS_SELECTOR,".xdj266r.x11i5rnm.xat24cr.x1mh8g0r.x1vvkbs.x126k92a, .x11i5rnm.xat24cr.x1mh8g0r.x1vvkbs.xtlvy1s.x126k92a, .x6s0dn4.x78zum5.xdt5ytf.x5yr21d.xl56j7k.x10l6tqk.x17qophe.x13vifvy.xh8yej3")
                text=""
                for element in elements:
                    try:
                        if element.text:
                         text = text + element.text
                         print("Found:", text)
                    except NoSuchElementException:
                        print("Element text not found")
            except NoSuchElementException:
                print("Element text not found")
            t=" "
            try:
                    element = self.driver.find_element(By.CSS_SELECTOR,"x1i10hfl.xjbqb8w.x6umtig.x1b1mbwd.xaqea5y.xav7gou.x9f619.x1ypdohk.xt0psk2.xe8uvvx.xdj266r.x11i5rnm.xat24cr.x1mh8g0r.xexx8yu.x4uap5.x18d9i69.xkhd6sd.x16tdsg8.x1hl2dhg.xggy1nq.x1a2a7pz.x1heor9g.xt0b8zv.xo1l8bm")
                    
                    if element.text:
                     t = t + element.text
                     print("Time:", t)
            except NoSuchElementException:
                    print("Element text not found")
            desc=title+" "+text+" "+t
           
            


             #Location
            b=BienImmobilier()
            location=""
            city=""
            if 'location_text' in body:
               start = 'location_text":{"delight_ranges":[],"image_ranges":[],"inline_style_ranges":[],"aggregated_ranges":[],"ranges":[],"color_ranges":[],"text":"'
               index = body.index(start)
               new_text=body[index+len(start):]
               end='"'
               endindex=new_text.index(end)
               location=new_text[:endindex]
               
               decoded_string = location.encode().decode('unicode_escape')
               city=decoded_string
               location=b.get_state(decoded_string,"tunisia")
            

             #price
            price=""
            if 'formatted_price' in body:
               start = '"formatted_price":{"text":"'
               index = body.index(start)
               new_text=body[index+len(start):]
               end=''

               price=new_text[:10]
               split_string = price.split("\\")
               price=split_string[0]
             




            # #images
            # print(images)  
            # print("\n__________________________")
            # #descp
            # print("desc: ")
            # print("title",title)
            # print(desc)
            # print("\n__________________________")
            # #temps
            # print("temps")
            # # print(Temps)
            # print("\n__________________________")
            # #temps
            # print("location")
            # print(location)
            # print("\n__________________________")   
            # #price
            # print("price")
            # print(price)
            
            # print("\n__________________________")  
            tt=""
            try:
             element = self.driver.find_element(By.ID, ":r19:")
             if element  and element.is_displayed():
            #   print(element.text)         
              element_text = element.get_attribute("textContent") 
            #   print(element_text)
              tt=tt+" "+element_text
            except NoSuchElementException:
             print("\nElement not found\n")
            try:
             element = self.driver.find_element(By.CLASS_NAME, "x193iq5w.xeuugli.x13faqbe.x1vvkbs.x1xmvt09.x1lliihq.x1s928wv.xhkezso.x1gmr53x.x1cpjm7i.x1fgarty.x1943h6x.x4zkp8e.x676frb.x1nxh6w3.x1sibtaa.xo1l8bm.xi81zsa.x1yc453h")
             if element  and element.is_displayed():
              print(element.text)         
              element_text = element.get_attribute("textContent") 
              print(element_text)
              tt=tt+" "+element_text
            except NoSuchElementException:
             print("\nElement not found\n")
            ###########Time
            Temps=""
            number=""
            datetype=""

            time.sleep(5)
            if '<div class="__fb-light-mode"><span' in body:
               start = '<div class="__fb-light-mode"><span'
               index = body.index(start)
               new_text=body[index:]

               Temps=new_text[len(start):50]
               
               t=Temps.split(">")
               temps=t[1].replace("<","")
               with open('file.txt', 'a') as file:
                file.write(Temps+"\n")

               x=temps.split(" ")
               if len(x)>1:
                number,datetype=temps.split(" ")
               
           

            else: pass
            time.sleep(10)

           
            comments=[]
            
            try:
             elements = self.driver.find_elements(By.CSS_SELECTOR, 'div[role="button"]')
             
            except NoSuchElementException:
             print("\nElement not found\n")

            matching_elements = [element for element in elements if "répondu"  in element.text or "réponse"  in element.text or "autre commentaire" in element.text]
            if matching_elements:
                 for element in matching_elements:
                    if element in elements:
                      try:
                       element.click()
                      except MoveTargetOutOfBoundsException as e:
                        print("MoveTargetOutOfBoundsException occurred:", e) 
                 time.sleep(3)
                 response = HtmlResponse(url=self.driver.current_url,body=self.driver.page_source,encoding='utf-8')    
                 body=self.driver.page_source
                 sel = scrapy.Selector(response)
                 time.sleep(1)
                 divs = response.xpath('//div[contains(@class, "xmjcpbm x1tlxs6b x1g8br2z x1gn5b1j x230xth x9f619 xzsf02u x1rg5ohu xdj266r x11i5rnm xat24cr x1mh8g0r x193iq5w x1mzt3pk x1n2onr6 xeaf4i8 x13faqbe")]')
                 
                 for div in divs:
                    text = div.xpath('.//text()').getall()
                    hrefs = div.xpath('.//@href').getall()
                    comment=""
                    userlink=""
                    
                    if 'Auteur' in text:
                        index = text.index('Auteur')
                        removed_element = text.pop(index)
                    comment = ' '.join(text[1:])
                    lien=hrefs[0].split("/")
                   
                    userlink="https://www.facebook.com/"+lien[4]
                    
                    pairs = { 'user': userlink, 'comment': comment } 
                    comments.append(pairs)


                    
                    
                    #scraped date 
            now = datetime.datetime.now()
            ScrapedDate = now.strftime("%d-%m-%Y %H:%M:%S") 
            row ={"url": response.url, "description": desc,"city":city,'state':location
                  ,'price': price,'image_urls': images,'ScrapedDate':ScrapedDate,'comments':comments }
            b=BienImmobilier()
            b.extractFBGrp(row)
            b.noneCheck()
            b.print_maison()
            b.SaveDb(b)
           
                    


    def parse_Pages_Profil_annonce(self,url):pass
